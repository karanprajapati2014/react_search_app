import './App.css';
import React, { useState, useEffect } from 'react';
import reactDom from 'react-dom';
import ProductView from './components/ProductView';
import Box from '@mui/material/Box';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Grid from '@mui/material/Grid';

// Main Function App
function App() {
  const [items, setItems] = useState([]);
  const [value, setValue] = React.useState('');
  const [error, setError] = React.useState('');

  // to get data from api based on search value
  const callItem = () => {
    fetch(`https://www.thecocktaildb.com/api/json/v1/1/search.php?s=${value}`)
      .then((res) => res.json())
      .then(
        (result) => {
          // set value with drinks parameter
          setItems(result.drinks);
        },
        (error) => {
          setError(error);
        },
      );
  };
  // to handle textinput changes value in handlechange function
  const handleChange = (event) => {
    setValue(event.target.value);
  };

  return (
    <Container className="App">
      <header className="App-header">
        <Typography gutterBottom>Search Listing</Typography>
      </header>
      <Box className="SearchBar">
        <TextField
          id="outlined-basic"
          label="Search"
          variant="outlined"
          value={value}
          onChange={handleChange}
        />
        <Button
          variant="outlined"
          onClick={() => {
            callItem();
          }}
        >
          Search
        </Button>
      </Box>

      <Box className="BoxView">
        <Grid container spacing={2}>
          {items.slice(0, 5).map((item) => {
            return <ProductView product={item} />;
          })}
        </Grid>
      </Box>
    </Container>
  );
}

export default App;
