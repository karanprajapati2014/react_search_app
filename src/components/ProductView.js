import React from 'react';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';
import Grid from '@mui/material/Grid';
import Link from '@mui/material/Link';

// ProductView Components for the View Product
const ProductView = ({ product }) => {
  return (
    <Grid item xs={4}>
      <Box key={product.idDrink} className="GridView">
        <img src={product.strDrinkThumb} alt={product.title} width={200} />
        <Typography variant="h6" gutterBottom component="div">
          {product.strDrink} ({product.strAlcoholic})
        </Typography>
        <Typography gutterBottom> {product.strInstructions}</Typography>

        <Typography gutterBottom>
          Ingredient - {product.strIngredient1}, {product.strIngredient2},
          {product.strIngredient3}, {product.strIngredient4},
          {product.strIngredient5}
        </Typography>
        <Typography gutterBottom>
          Measure - {product.strMeasure1}, {product.strMeasure2},
          {product.strMeasure3}, {product.strMeasure4}, {product.strMeasure5}
        </Typography>
      </Box>
    </Grid>
  );
};

export default ProductView;
